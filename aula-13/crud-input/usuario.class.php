<?php
    class usuario{
        private $nome;
        private $email;
        private $senha;

        public function __construct(){
            $this->db = new PDO('mysql:host=localhost:3307;dbname=aula_php', 'root', '');

            if($this->db){
                echo "entrei";
            }else {
                echo "nao entrei";
            }
        }

        public function setNome(string $nome){
            $this->nome = $nome;
        }

        public function setEmail(string $email){
            $this->email = $email;
        }

        public function setSenha(string $senha){
            $this->senha = password_hash($senha, PASSWORD_DEFAULT);
        }

        // public function getId(int $id): int {
        //     return $this->id;
        // }

        // public function getNome(string $nome): string {
        //     return $this->nome;
        // }

        // public function getEmail(string $emial): string {
        //     return $this->email;
        // }

        // public function getSenha(string $senha): string {
        //     return $this->senha;
        // }

        public function getUser(int $id) : int{
            return array(
                $this->id,
                $this->nome,
                $this->email,
            );
        } 

        
        public function saveUsuario(){
            $conn = $this->db->prepare('insert into tb_usuario values (null, ?, ?, ?)');
            $conn->bindParam(1, $this->nome, PDO::PARAM_STR);
            $conn->bindParam(2, $this->email, PDO::PARAM_STR);
            $conn->bindParam(3, $this->senha, PDO::PARAM_STR);
            
            $ret = $conn->execute();
            
            if($ret){
                return true;
            }else{
                return false;
            }
        }

        public function deleteUsuario($id){
            $conn = $this->db->prepare('delete from tb_usuario where cd = ?');
            $conn->bindParam(1, $id, PDO::PARAM_INT);
            $conn->execute();
        }

        public function listUsuarios(){
            $conn = $this->db->query('select * from tb_usuario');
            
            while($ret = $conn->fetch(PDO::FETCH_OBJ)){
                echo "ID : ". $ret->cd ." | nome : " . $ret->nome . " | Email : " . $ret->email . "\n"; 
            }
        }
        
        public function __destruct(){
            unset($this->db);
        }
    }