<?php
echo "<pre>";
//contante no php 

define('QTD_PAGINAS', 10);

echo "o valor da minha contante é " . QTD_PAGINAS ;

//variavel termos o ip do banco 
$ip_banco = "192.168.0.45";

define('IP_BANCO', $ip_banco);
echo "\nO ip do Banco da nasa é " . IP_BANCO;

//constantes magicas 
echo "\nestou na linha: " . __LINE__;
echo "\nestou na linha: " . __LINE__;
echo "\nO nome do arquivo: " . __FILE__ . "\n";

var_dump(IP_BANCO); 

$semana = [1,2,3,4,5,6,7];

var_dump($semana);

echo "\n\n\n";

//unset($semana); //destroi a variavel;

$seman = ['seg', 'ter', 'qua', 'qui', 'sex', 'sab', 'dom'];

// Destroi a variavel
unset($dias);

// Array V2
$dias[0] = 'dom';
$dias[1] = 'seg';
$dias[2] = 'ter';
$dias[3] = 'qua';
$dias[4] = 'qui';
$dias[5] = 'sex';
$dias[6] = 'sab';

// Destroi a variavel
unset($dias);

// Array V3
$dias = array(
    0 => 'dom',
    1 => 'seg',
    2 => 'ter',
    3 => 'qua',
    4 => 'qui',
    5 => 'sex',
    6 => 'sab'
);

var_dump($dias);